
      // getUserMedia only works for secure pages
      // TODO: activate in prod
      // if (!/https/.test(window.location.protocol)) window.location.protocol = 'https://';

      class App extends React.Component {

        constructor(props) {
          super(props);
          this.state = {
            screenshot: null,
            tab: 0,
          };
        }

        handleClick = () => {
                  var resizedCanvas = document.createElement("canvas");
                          var resizedContext = resizedCanvas.getContext("2d");

                                  resizedCanvas.height = "320";
                                          resizedCanvas.width = "240";

                                                  const canvas = this.webcam.getCanvas();
                                                          var context = canvas.getContext("2d");

                                                                  resizedContext.drawImage(canvas, 0, 0, 320, 240);
                                                                          var screenshot = resizedCanvas.toDataURL("image/jpeg", 0.92);

                                                                                  console.log(screenshot);


          const params = {
          path: "/api/privaye/",
          serviceName: "detection_600"
          };

          const postData = {
              service: params.serviceName,
                  data: [ screenshot.replace(/^data:image\/\w+;base64,/, "") ],
                      parameters: {
                      input: {},
                      output:{
                      confidence_threshold: 0.3,
                      bbox: true
                      },
                      mllib: {}
                      }
                      }
          axios.post('/api/private/predict', postData)
            .then((response) => {

  console.log(response);
            console.log(response.data.body.predictions[0]);
            if(response.data.body.predictions[0]) {
            const classes = response.data.body.predictions[0].classes;
            this.setState({
              boxes: classes.map( c => {
                return [
                  c.bbox.xmin *2,
                  c.bbox.ymin *2,
                  c.bbox.xmax *2 - c.bbox.xmin *2,
                  c.bbox.ymax *2 - c.bbox.ymin *2
                ]
              })
            });

            classes.forEach( (c, index) => {
            console.log(c);
            });
            this.handleClick();
            }
                  });
        }

        render() {
          return (
            <div>
              <h1>webcam-example
              <button onClick={this.handleClick}>Start</button>
              </h1>
              <Webcam
                audio={false}
                ref={node => this.webcam = node}
              />
              <Boundingbox id="boxes" image="webcam.png"
                           boxes={this.state.boxes}
                           options={{
                           colors: {
                                     normal: 'rgba(255,0,0,1)'
                                     }}}
              />
            </div>
          );
        }
      }

      ReactDOM.render(<App />, document.getElementById('root'));
